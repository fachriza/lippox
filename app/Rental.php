<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rentals';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car-id',
        'client-id',
        'date-from',
        'date-to'
    ];

    /**
     * Get the Car record associated with the Rental.
     */
    public function car()
    {
        return $this->hasOne('App\Car', 'id', 'car-id');
    }
}
