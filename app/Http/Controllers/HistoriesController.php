<?php

namespace App\Http\Controllers;

use Validator;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HistoriesController extends Controller
{
    public function client(Request $request, $id){
        try{
            $client = Client::find($id);
            if(count($client) < 1){
                return (new Response(
                    json_encode(["message"=>"client is empty"]), 404))
                    ->header('Content-Type', "application/json");
            }

            $histories = [];
            if(isset($client->rental) && !empty($client->rental)){
                foreach($client->rental as $rental){
                    $histories[] = [
                        "brand" => $rental->car->brand,
                        "type"  => $rental->car->type,
                        "plate" => $rental->car->plate,
                        "date-from" => $rental->{'date-from'},
                        "date-to"   => $rental->{'date-to'}
                    ];
                }
            }

            $data = [
                "id" => $client->id,
                "name" => $client->name,
                "gender" => $client->gender,
                "histories" => $histories
            ];

            return (new Response(json_encode($data), 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }
}
