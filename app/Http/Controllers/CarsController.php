<?php

namespace App\Http\Controllers;

use Validator;
use App\Car;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CarsController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->json()->get("data");
        //if data is empty return 
        if(empty($data)){
            return (new Response(json_encode(["message"=>"data is empty"]), 400))
                ->header('Content-Type', "application/json");
        }


        //validate data
        $validator = Validator::make($data, [
            'brand' => 'required',
            'type'  => 'required',
            'year'  => 'required|before:'.date('Y', strtotime('+1 year')),
            'color' => 'required',
            'plate' => 'required|unique:cars'
        ]);

        if ($validator->fails()){
            return (new Response(json_encode($validator->errors()), 400))
                ->header('Content-Type', "application/json");
        }

        //create car
        try{
            $insert = Car::create($data);
            return (new Response(json_encode(["id"=>$insert->id]), 200))
                                                  ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function update(Request $request, $id)
    {
        //find data
        $car = Car::findOrFail($id);
        if(empty($car)){
            return (new Response(json_encode(["message"=>"car is empty"]), 404))
                                                       ->header('Content-Type', "application/json");
        }

        $data = $request->json()->get("data");
        //if data is empty return 
        if(empty($data)){
            return (new Response(json_encode(["message"=>"data is empty"]), 400))
            ->header('Content-Type', "application/json");
        }


        //validate data
        $validator = Validator::make($data, [
            'brand' => 'required',
            'type'  => 'required',
            'year'  => 'required|before:'.date('Y', strtotime('+1 year')),
            'color' => 'required',
            'plate' => 'required|unique:cars'
        ]);

        if ($validator->fails()){
            return (new Response(json_encode($validator->errors()), 400))
                ->header('Content-Type', "application/json");
        }

        //update car
        try{
            $car->fill($data)->save();
            return (new Response("", 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function delete(Request $request, $id){
        //find data
        $car = Car::findOrFail($id);
        if(empty($car)){
            return (new Response(json_encode(["message"=>"car is empty"]), 404))
                                                       ->header('Content-Type', "application/json");
        }

        //delete car
        try{
            $car->delete();
            return (new Response("", 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function list(Request $request){
        //list cars
        try{
            $cars = Car::all();
            if(count($cars) < 1){
                return (new Response(json_encode(["message"=>"car is empty"]), 404))
                                                           ->header('Content-Type', "application/json");
            }

            return (new Response(json_encode($cars), 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }
}
