<?php

namespace App\Http\Controllers;

use Validator;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClientsController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->json()->get("data");
        //if data is empty return 
        if(empty($data)){
            return (new Response(json_encode(["message"=>"data is empty"]), 400))
                ->header('Content-Type', "application/json");
        }


        //validate data
        $validator = Validator::make($data, [
            'name' => 'required',
            'gender'  => 'required|in:male,female'
        ]);

        if ($validator->fails()){
            return (new Response(json_encode($validator->errors()), 400))
                ->header('Content-Type', "application/json");
        }

        //create client
        try{
            $insert = Client::create($data);
            return (new Response(json_encode(["id"=>$insert->id]), 200))
                                                  ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function update(Request $request, $id)
    {
        //find data
        $client = Client::findOrFail($id);
        if(empty($client)){
            return (new Response(json_encode(["message"=>"client is empty"]), 404))
                                                       ->header('Content-Type', "application/json");
        }

        $data = $request->json()->get("data");
        //if data is empty return 
        if(empty($data)){
            return (new Response(json_encode(["message"=>"data is empty"]), 400))
            ->header('Content-Type', "application/json");
        }


        //validate data
        $validator = Validator::make($data, [
            'name' => 'required',
            'gender'  => 'required|in:male,female'
        ]);

        if ($validator->fails()){
            return (new Response(json_encode($validator->errors()), 400))
                ->header('Content-Type', "application/json");
        }

        //update client
        try{
            $client->fill($data)->save();
            return (new Response("", 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function delete(Request $request, $id){
        //find data
        $client = Client::findOrFail($id);
        if(empty($client)){
            return (new Response(json_encode(["message"=>"client is empty"]), 404))
                                                       ->header('Content-Type', "application/json");
        }

        //delete client
        try{
            $client->delete();
            return (new Response("", 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function list(Request $request){
        //list clients
        try{
            $clients = Client::all();
            if(count($clients) < 1){
                return (new Response(json_encode(["message"=>"client is empty"]), 404))
                                                           ->header('Content-Type', "application/json");
            }

            return (new Response(json_encode($clients), 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }
}
