<?php

namespace App\Http\Controllers;

use Validator;
use App\Rental;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RentalsController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->json()->get("data");
        //if data is empty return 
        if(empty($data)){
            return (new Response(json_encode(["message"=>"data is empty"]), 400))
                ->header('Content-Type', "application/json");
        }

        $from = $data["date-from"];
        $to = $data["date-to"];

        //validate data
        $validator = Validator::make($data, [
            'car-id'     => 'required|exists:cars,id',
            'client-id'  => 'required|exists:clients,id',
            'date-from'  => 'required|date|after:tomorrow|before:'.date("Y-m-d", strtotime("+7days")),
            'date-to' => 'required|date|after:'.date("Y-m-d", strtotime($from."-1day")).'|before:'.date("Y-m-d", strtotime($from. "+3day"))
        ]);

        if ($validator->fails()){
            return (new Response(json_encode($validator->errors()), 400))
                ->header('Content-Type', "application/json");
        }


        //check Car is not rented at selected rent date
        $rentaled = Rental::where("car-id", $data["car-id"])
                  ->whereBetween("date-from", [$from, $to])
                  ->whereBetween("date-to", [$from, $to])
                  ->count();

        if($rentaled > 1){
            return (new Response(json_encode(
                ["message"=>" Car can't rented at selected rent date"]), 400))
                ->header('Content-Type', "application/json");
        }

        //create rental
        try{
            $insert = Rental::create($data);
            return (new Response(json_encode(["id"=>$insert->id]), 200))
                                                  ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function update(Request $request, $id)
    {
        //find data
        $rental = Rental::findOrFail($id);
        if(empty($rental)){
            return (new Response(json_encode(["message"=>"rental is empty"]), 404))
                                                       ->header('Content-Type', "application/json");
        }

        $data = $request->json()->get("data");
        //if data is empty return 
        if(empty($data)){
            return (new Response(json_encode(["message"=>"data is empty"]), 400))
            ->header('Content-Type', "application/json");
        }

        $from = $data["date-from"];
        $to = $data["date-to"];

        //validate data
        $validator = Validator::make($data, [
            'car-id'     => 'required|exists:cars,id',
            'client-id'  => 'required|exists:clients,id',
            'date-from'  => 'required|date|after:tomorrow|before:'.date("Y-m-d", strtotime("+7days")),
            'date-to' => 'required|date|after:'.date("Y-m-d", strtotime($from."-1day")).'|before:'.date("Y-m-d", strtotime($from. "+3day"))
        ]);

        if ($validator->fails()){
            return (new Response(json_encode($validator->errors()), 400))
                ->header('Content-Type', "application/json");
        }

        //check Car is not rented at selected rent date
        $rentaled = Rental::where("car-id", $data["car-id"])
                  ->whereBetween("date-from", [$from, $to])
                  ->whereBetween("date-to", [$from, $to])
                  ->count();

        if($rentaled > 1){
            return (new Response(json_encode(
                ["message"=>" Car can't rented at selected rent date"]), 400))
                ->header('Content-Type', "application/json");
        }

        //update rental
        try{
            $rental->fill($data)->save();
            return (new Response("", 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function delete(Request $request, $id){
        //find data
        $rental = Rental::findOrFail($id);
        if(empty($rental)){
            return (new Response(json_encode(["message"=>"rental is empty"]), 404))
                                                       ->header('Content-Type', "application/json");
        }

        //delete rental
        try{
            $rental->delete();
            return (new Response("", 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }

    public function list(Request $request){
        //list rentals
        try{
            $rentals = Rental::all();
            if(count($rentals) < 1){
                return (new Response(json_encode(["message"=>"rental is empty"]), 404))
                                                           ->header('Content-Type', "application/json");
            }

            return (new Response(json_encode($rentals), 200))
                ->header('Content-Type', "application/json");
        }catch(Exception $e){
            return (new Response(json_encode([$e->getMessage()]), 500))
                ->header('Content-Type', "application/json");
        }
    }
}
