<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'cars'], function () use ($app) {
    $app->post('/', 'App\Http\Controllers\CarsController@create');
    $app->put('/{id}', 'App\Http\Controllers\CarsController@update');
    $app->delete('/{id}', 'App\Http\Controllers\CarsController@delete');
    $app->get('/', 'App\Http\Controllers\CarsController@list');
});

$app->group(['prefix' => 'clients'], function () use ($app) {
    $app->post('/', 'App\Http\Controllers\ClientsController@create');
    $app->put('/{id}', 'App\Http\Controllers\ClientsController@update');
    $app->delete('/{id}', 'App\Http\Controllers\ClientsController@delete');
    $app->get('/', 'App\Http\Controllers\ClientsController@list');
});

$app->group(['prefix' => 'rentals'], function () use ($app) {
    $app->post('/', 'App\Http\Controllers\RentalsController@create');
    $app->put('/{id}', 'App\Http\Controllers\RentalsController@update');
    $app->delete('/{id}', 'App\Http\Controllers\RentalsController@delete');
    $app->get('/', 'App\Http\Controllers\RentalsController@list');
});

$app->group(['prefix' => 'histories'], function () use ($app) {
    $app->get('/client/{id}', 'App\Http\Controllers\HistoriesController@client');
});